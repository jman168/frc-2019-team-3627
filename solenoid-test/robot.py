import wpilib, magicbot
# from pyconsole_util import util
from robotmap import RobotMap as Map
# from components.drive import Drive

class Robot(magicbot.MagicRobot):

    def createObjects(self):
        self.testSolenoid1 = wpilib.Solenoid(Map.solenoids.testSolenoid1)
        self.testSolenoid2 = wpilib.Solenoid(Map.solenoids.testSolenoid2)
        self.testSolenoid3 = wpilib.Solenoid(Map.solenoids.testSolenoid3)
        self.testSolenoid4 = wpilib.Solenoid(Map.solenoids.testSolenoid4)

        # ==== SETUP JOYSTICKS ====
        self.xbox = wpilib.XboxController(0)

    def disabledPeriodic(self):
        pass

    def teleopInit(self):
        pass

    def teleopPeriodic(self):
        a_pressed = self.xbox.getAButton()
        x_pressed = self.xbox.getXButton()
        y_pressed = self.xbox.getYButton()
        b_pressed = self.xbox.getBButton()

        if a_pressed:
            self.testSolenoid1.set(True)
            print("a")
        else:
            self.testSolenoid1.set(False)

        if b_pressed:
            self.testSolenoid2.set(True)
            print("b")
        else:
            self.testSolenoid2.set(False)

        if x_pressed:
            self.testSolenoid3.set(True)
            print("x")
        else:
            self.testSolenoid3.set(False)

        if y_pressed:
            self.testSolenoid4.set(True)
            print("y")
        else:
            self.testSolenoid4.set(False)

if __name__ == "__main__":
    wpilib.run(Robot, physics_enabled=True)
