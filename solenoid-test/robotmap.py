'''
This script serves to create a list of ports and channels that can be easily
changed if someone were to say- change the port a motor was plugged into on the
RoboRIO
'''
class Solenoids:
    testSolenoid1 = 0
    testSolenoid2 = 1
    testSolenoid3 = 2
    testSolenoid4 = 3

class RobotMap:
    solenoids = Solenoids()

# Used for testing if the RobotMap works
def test():
    print(RobotMap.solenoids)

# Doesn't run in final code
if __name__ == "__main__":
    test()
