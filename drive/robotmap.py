'''
This script serves to create a list of ports and channels that can be easily
changed if someone were to say- change the port a motor was plugged into on the
RoboRIO
'''

# List of ports for motor controllers used for driving on the RoboRIO (feeds into PWM)
class Drive:
    # It's supposed to be a CANTalon but I don't know what that is- TalonSR, TalonSRX, or a canified Talon idk
    FR_CANTALON_MC = 2
    FL_CANTALON_MC = 3
    BR_CANTALON_MC = 5
    BL_CANTALON_MC = 4

# List of PWM ports on the RoboRIO (feeds into RobotMap)
class CAN:
    drive = Drive()

# The full map/list of ports
class RobotMap:
    can = CAN()




# Used for testing if the RobotMap works
def test():
    print(RobotMap.can.drive.FR_CANTALON_MC)

# Doesn't run in final code
if __name__ == "__main__":
    test()
