import ctre, time, math
accelFact = 0.01

class robotMotor:
    def __init__(self, channel):
        self.motor = ctre.WPI_TalonSRX(channel)
        self.setSpeed = 0.0
        self.curSpeed = 0.0
        self.lastTime = time.clock()

    def set(self,speed):
        self.setSpeed = speed

    def get(self):
        return self.setSpeed

    def update(self):
        curTime = time.clock()
        timeDif = (curTime - self.lastTime) * 1000
        self.curSpeed += (self.setSpeed-self.curSpeed) * accelFact * timeDif
        self.motor.set(self.curSpeed)
        self.lastTime = curTime
