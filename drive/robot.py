import wpilib, magicbot, ctre
from robotMotor import robotMotor as robotMotor
# from pyconsole_util import util
from robotmap import RobotMap as Map
from wpilib.drive import MecanumDrive
# from components.drive import Drive


class Robot(magicbot.MagicRobot):
    def createObjects(self):
        self.frontRightMotor = robotMotor(Map.can.drive.FR_CANTALON_MC)
        self.frontLeftMotor = robotMotor(Map.can.drive.FL_CANTALON_MC)
        self.rearRightMotor = robotMotor(Map.can.drive.BR_CANTALON_MC)
        self.rearLeftMotor = robotMotor(Map.can.drive.BL_CANTALON_MC)

        self.hatchSolenoid = wpilib.DoubleSolenoid(forwardChannel=0,reverseChannel=1)
        self.armMotor = ctre.WPI_VictorSPX(6)

        self.drive = MecanumDrive(
            self.frontLeftMotor,
            self.rearLeftMotor,
            self.frontRightMotor,
            self.rearRightMotor,
        )

        self.drive.setExpiration(0.1)

        # ==== SETUP JOYSTICKS ====
        self.xbox = wpilib.XboxController(0)

        self.power = 1.0
        self.reverse = False

        self.gyro = wpilib.AnalogGyro(0)
        self.gyro.calibrate()

    def disabledPeriodic(self):
        pass

    def teleopInit(self):
        pass

    def teleopPeriodic(self):
        ######### DRIVE CODE ############

        gyroAngle = self.gyro.getAngle()

        if self.reverse:
            self.drive.driveCartesian(
                -self.xbox.getX(0)*self.power, self.xbox.getY(0)*self.power, self.xbox.getX(1)*self.power, gyroAngle
            )
        else:
            self.drive.driveCartesian(
                self.xbox.getX(0)*self.power, -self.xbox.getY(0)*self.power, self.xbox.getX(1)*self.power, gyroAngle
            )

        # self.frontRightMotor.set(self.xbox.getX(0))
        self.frontRightMotor.update();
        self.frontLeftMotor.update();
        self.rearRightMotor.update();
        self.rearLeftMotor.update();

        if self.xbox.getStickButtonPressed(0):
            print("switch")
            if self.power == 1.0:
                self.power = 0.35
            elif self.power == 0.35:
                self.power = 1.0

        if self.xbox.getStickButtonPressed(1):
             self.gyro.reset()

        ####### HATCH CODE ########

        if self.xbox.getAButtonPressed():
            if self.hatchSolenoid.get() == 1:
                self.hatchSolenoid.set(2)
                print("Solenoid status 2!")
            elif self.hatchSolenoid.get() == 2:
                self.hatchSolenoid.set(1)
                print("Solenoid status 1!")
            elif self.hatchSolenoid.get() == 0:
                self.hatchSolenoid.set(1)
                print("Solenoid status 1!")

        leftVal = self.xbox.getTriggerAxis(0)
        rightVal = self.xbox.getTriggerAxis(1)

        val = rightVal-leftVal

        self.armMotor.set(val)


if __name__ == "__main__":
    wpilib.run(Robot, physics_enabled=True)
