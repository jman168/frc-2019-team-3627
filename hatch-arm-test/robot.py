import wpilib, magicbot, ctre

class Robot(magicbot.MagicRobot):

    def createObjects(self):
        self.solenoid = wpilib.DoubleSolenoid(forwardChannel=0,reverseChannel=1)

        self.armMotor = ctre.WPI_VictorSPX(6)

        # ==== SETUP JOYSTICKS ====
        self.xbox = wpilib.XboxController(0)


    def disabledPeriodic(self):
        pass

    def teleopInit(self):
        pass

    def teleopPeriodic(self):
        if self.xbox.getAButtonPressed():
            if self.solenoid.get() == 1:
                self.solenoid.set(2)
                print("Solenoid status 2!")
            elif self.solenoid.get() == 2:
                self.solenoid.set(1)
                print("Solenoid status 1!")
            elif self.solenoid.get() == 0:
                self.solenoid.set(1)
                print("Solenoid status 1!")

        leftVal = (self.xbox.getTriggerAxis(0)+1)/2
        rightVal = (self.xbox.getTriggerAxis(1)+1)/2

        print(leftVal,rightVal)

        val = (-leftVal)+rightVal

        self.armMotor.set(val)
        print(val)



if __name__ == "__main__":
    wpilib.run(Robot, physics_enabled=True)
