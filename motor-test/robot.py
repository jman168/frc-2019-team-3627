import wpilib, magicbot, ctre

class Robot(magicbot.MagicRobot):

    def createObjects(self):
        self.solenoid = wpilib.DoubleSolenoid(forwardChannel=0,reverseChannel=1)

        self.armMotor = ctre.WPI_VictorSPX(6)

        # ==== SETUP JOYSTICKS ====
        self.xbox = wpilib.XboxController(0)


    def disabledPeriodic(self):
        pass

    def teleopInit(self):
        # self.armMotor.configFactoryDefault()
        pass

    def teleopPeriodic(self):
        val = self.xbox.getY(0)
        self.armMotor.set(val)
        print("set power (percent):",val,"output power (percent):",self.armMotor.getMotorOutputPercent(),"bus voltage (volts):",self.armMotor.getBusVoltage(),"output voltage (volts):",self.armMotor.getMotorOutputVoltage(),"output current (amps):",self.armMotor.getOutputCurrent(),"controller temperature (celsius):",self.armMotor.getTemperature())



if __name__ == "__main__":
    wpilib.run(Robot, physics_enabled=True)
